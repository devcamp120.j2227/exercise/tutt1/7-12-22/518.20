import { Component } from "react";
import axios from "axios";

class AxiosLibrary extends Component {

    axiosLibraryCallAPI = async (config) => {
        let response = await axios(config);

        return response
    }

    getByIDHandler = () => {
        var config = {
            method: 'get',
            url: 'https://jsonplaceholder.typicode.com/posts/1',
            headers: { }
        };

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    getAllHandler = () => {
        var config = {
            method: 'get',
            url: 'https://jsonplaceholder.typicode.com/posts',
            headers: { }
        };

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    postHandler = () => {
        var data = JSON.stringify({
            "userId": 1,
            "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
            "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
        });
        
        var config = {
            method: 'post',
            url: 'https://jsonplaceholder.typicode.com/posts',
            headers: { 
                'Content-Type': 'application/json'
            },
            data : data
        };
          

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    updateHandler = () => {
        var data = JSON.stringify({
            "userId": 2,
            "title": "Test",
            "body": "Test"
        });
        
        var config = {
            method: 'put',
            url: 'https://jsonplaceholder.typicode.com/posts/1',
            headers: { 
                'Content-Type': 'application/json'
            },
            data : data
        };

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    deleteHandler = () => {
        var config = {
            method: 'delete',
            url: 'https://jsonplaceholder.typicode.com/posts/1',
            headers: { }
        };

        this.axiosLibraryCallAPI(config)
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    render() {
        return (
            <div className="row">
                <div className="col"><button className="btn btn-primary" onClick={this.getByIDHandler}>Get by ID</button></div>
                <div className="col"><button className="btn btn-primary" onClick={this.getAllHandler}>Get All</button></div>
                <div className="col"><button className="btn btn-primary" onClick={this.postHandler}>Post Create</button></div>
                <div className="col"><button className="btn btn-primary" onClick={this.updateHandler}>Put Update</button></div>
                <div className="col"><button className="btn btn-primary" onClick={this.deleteHandler}>Delete</button></div>
            </div>
        )
    }
}

export default AxiosLibrary;