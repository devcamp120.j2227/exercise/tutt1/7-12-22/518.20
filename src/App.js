import AxiosLibrary from "./components/AxiosLibrary";
import FetchAPI from "./components/FetchAPI";

function App() {
  return (
    <div>
      <FetchAPI/>
      <hr/>
      <AxiosLibrary/>
    </div>
  );
}

export default App;
